package com.oehm4;

import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;


public class HqlLaptopDAO<AccountDTO> {

	public List<HqlLaptopDTO> getLaptopDetails()
	{
	 Configuration configuration = new Configuration();
	    configuration.configure();
	    configuration.addAnnotatedClass(HqlLaptopDTO.class);
	    SessionFactory sessionFactory = configuration.buildSessionFactory();
	    Session session = sessionFactory.openSession();
	    
	    String hql  = "from HqlLaptopDTO ";
	    
	    Query query = session.createQuery(hql);
	   
	    
	    List<HqlLaptopDTO> list = query.list();
	
	    return list;
	    
	
	}   
	
	public HqlLaptopDTO getDetailsByPrice( Double price)
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		configuration.addAnnotatedClass(HqlLaptopDTO.class);
		 SessionFactory sessionFactory = configuration.buildSessionFactory();
		  Session session = sessionFactory.openSession();
		  Transaction transaction = session.beginTransaction();
		  String hql = "from HqlLaptopDTO where price=:newPrice";
		  Query query = session.createQuery(hql);
		  query.setParameter("newPrice", price);
		  HqlLaptopDTO uniqueResult = (HqlLaptopDTO) query.uniqueResult();
		  return uniqueResult;
	}
	
	
	public void updatePriceById (Double newPrice, Long id)
	{
		
		Configuration configuration = new Configuration();
		configuration.configure();
		configuration.addAnnotatedClass(HqlLaptopDTO.class);
		 SessionFactory sessionFactory = configuration.buildSessionFactory();
		  Session session = sessionFactory.openSession();
		  Transaction transaction = session.beginTransaction();
		  String hql  = "update HqlLaptopDTO set price=:newPrice where id=:id";
		  Query query = session.createQuery(hql);
		  query.setParameter("newPrice", newPrice);
		  query.setParameter("id", id);
		  int updatedRows = query.executeUpdate();
		  transaction.commit();
		  if(updatedRows==0)
		  {
			  System.out.println("Update Operation Failed");
			  return;
		  }
		 
			  System.out.println("Update Operation Successfull");
		
		  
		
	}
	
	public void deleteByBrand(Long id)
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		configuration.addAnnotatedClass(HqlLaptopDTO.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
	    Session session = sessionFactory.openSession();
	    Transaction transaction = session.beginTransaction();
		String hql = "delete HqlLaptopDTO where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		int updatedRows = query.executeUpdate();
		
		if(updatedRows==0)
		{
			System.out.println("Delete Operation Failed");
			return;
		}
	    System.out.println("Delete Operation Successfull");
	    
	}
}
