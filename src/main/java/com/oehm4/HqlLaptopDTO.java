package com.oehm4;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "laptop_details")


public class HqlLaptopDTO  implements Serializable{

	
	  @Id
	   
	    @Column(name = "ID")
	 	private Long id;
		
	    @Column(name = "SERIES")
	    private String series;
	    
	    @Column(name = "SIZE")
		private String size;
		     
	    @Column(name = "BRAND")
		private String brand;
		
	    
	    @Column(name = "OPERATING_SYSTEM")
		private String operatingSystem;
		
	    
	    @Column(name = "CPU")
		private String cpu;
		
	    @Column(name = "RAM")
		private String ram;
		
	    @Column(name = "STORAGE")
		private String storage;
		
	    @Column(name = "PRICE")
		private Double price;

	    @Column(name = "COLOR")
	    private String color;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getSeries() {
			return series;
		}

		public void setSeries(String series) {
			this.series = series;
		}

		public String getSize() {
			return size;
		}

		public void setSize(String size) {
			this.size = size;
		}

		public String getBrand() {
			return brand;
		}

		public void setBrand(String brand) {
			this.brand = brand;
		}

		public String getOperatingSystem() {
			return operatingSystem;
		}

		public void setOperatingSystem(String operatingSystem) {
			this.operatingSystem = operatingSystem;
		}

		public String getCpu() {
			return cpu;
		}

		public void setCpu(String cpu) {
			this.cpu = cpu;
		}

		public String getRam() {
			return ram;
		}

		public void setRam(String ram) {
			this.ram = ram;
		}

		public String getStorage() {
			return storage;
		}

		public void setStorage(String storage) {
			this.storage = storage;
		}

		public Double getPrice() {
			return price;
		}

		public void setPrice(Double price) {
			this.price = price;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		@Override
		public String toString() {
			return "HqlLaptopDTO [id=" + id + ", series=" + series + ", size=" + size + ", brand=" + brand
					+ ", operatingSystem=" + operatingSystem + ", cpu=" + cpu + ", ram=" + ram + ", storage=" + storage
					+ ", price=" + price + ", color=" + color + "]";
		}
	    
	    
}
